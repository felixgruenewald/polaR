#' Party System Dispersion
#'
#' This function computes the dispersion of party systems. It is the square root of the variance of party scores.
#' For more information, see the \href{https://polarization.wiki/measures/dispersion}{encyclopedia entry on (Weighted) Party System Dispersion}
#'
#' Compatible Datasets: CSES
#'
#' @param dataset Dataframe
#' @param issue Issue dimension
#'
#' @return Aggregate dataset wih country/year level dispersion score.
#' @export
#'
#' @examples dispersion_cses5 <- dispersion(cses_5, "leftright")
dispersion <- function(dataset, issue, source = NA){

  measure <- "dispersion"
  label_measure <- "Party System Dispersion"
  dataset_string <- deparse(substitute(dataset))
  source <- ifelse(is.na(source), "survey", source)

  # Dispersion is the Root of the variance measure, therfore compute variance first
  dataset <- variance(dataset, issue)

  dataset$pol_score <- sqrt(dataset$pol_score)

  df <- fill_template_df(dataset, dataset_string, measure, issue, label_measure, source = source)

  return(df)
}
