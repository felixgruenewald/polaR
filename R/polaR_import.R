#' polaR Dataset Import
#'
#' Imports datasets and puts them into a coherent format for use with the polaR package.
#' Takes the dta files form the original sources and renames and formats package-relevant variables.
#'
#' # Source
#' Different datasets use different variable names and scales for the same items. The package therefore contains an import function that takes a range of datasets and recodes names, scales and missing values into a consistent pattern on the base of which the measures can be computed.
#'
#' For this, a pre-defined dictionary containing all relevant variables is used to look up the variable names for the dataset specified under the `source` option, and transforms them accordingly.
#'
#' As of now, the following datasets are available:
#'
#'   | Dataset        |  `source`   |
#'   |----------------|------------:|
#'   | CSES IMD       |  `cses_imd` |
#'   | CSES 5         |  `cses_5`   |
#'   | ESS            |  `ess`      |
#'   | CHES           |  `ches`     |
#'   | MARPOR         |  `cmp`      |
#'   | Eurobarometer  |  `eb`       |
#'
#' `polaR::var_dict` shows the renaming pattern and variables used for this. You can load and adapt or add to the dictionary to increase the number of variables or datasets compatible with the package.
#'
#' ## ESS
#' *Note*: The ESS is a difficult case, as the waves are available individually. Some of these waves do not contain the relevant variables for the polaR functions and therefore, the measures won't compute on these datasets. To generate the data from the encyclopedia, we merged the 10 waves together:
#'
#' ```r
#' ess1  <- polaR_import(source = "ess", path = "path/to/ess1.dta", keep_all = F)
#' ess2  <- polaR_import(source = "ess", path = "path/to/ess2.dta", keep_all = F)
#'
#' ...
#'
#' ess10 <- polaR_import(source = "ess", path = "path/to/ess10.dta", keep_all = F)
#' ess <-  rbind(ess1, ess2, ess3, ess4, ess5, ess6, ess7, ess8, ess9, ess10)
#' rm(ess1, ess2, ess3, ess4, ess5, ess6, ess7, ess8, ess9, ess10)
#' ```
#'
#' @md
#'
#' @importFrom haven zap_labels read_stata
#' @importFrom dplyr select starts_with
#'
#' @param path File path string to a .dta file
#' @param source The source of the data (cses_imd, cses_5, ess, eb...)
#' @param keep_all defaults to TRUE, keep all variables or reduce the dataset size to the package relevant variables
#'
#' @return Dataframe that is ready for use with other functions
#'
#' @examples polaR_import(source = "cses_imd", path = "data/cses_imd.dta")
#' @export
polaR_import <- function(source, path, keep_all = TRUE){
  dataset <- read_stata(path) %>%
    zap_labels()

  # Rename Variables into corresponding package vars, see polaR:::var_dict
  dataset <- rename_variables(dataset, paste(source))

  # Code Year variables
  dataset <- code_years(dataset)

  # reformat missings to NA
  dataset <- recode_missings(dataset, source)

  dataset <- country_codes(dataset, source)


  if (keep_all == FALSE){
    # ESS is too big, filtering only variables occurring in the dataset
      dataset <- dataset %>%
        select(starts_with(var_dict$name_dict))
  }

  # Normalize Expert Evaluations

  var_names <- c("leftright_expert_party_", "galtan_expert_party_", "lrecon_expert_party_",
               "leftright_expert_party_A", "leftright_expert_party_B", "leftright_expert_party_C", "leftright_expert_party_D", "leftright_expert_party_E", "leftright_expert_party_F", "leftright_expert_party_G", "leftright_expert_party_H", "leftright_expert_party_I",
                 "leftright_party_A", "leftright_party_B", "leftright_party_C", "leftright_party_D", "leftright_party_E", "leftright_party_F", "leftright_party_G", "leftright_party_H", "leftright_party_I")

    for (i in var_names){
      if (i %in% colnames(dataset)){
        min <- min(dataset[[i]], na.rm = TRUE)
        max <- max(dataset[[i]], na.rm = TRUE)

        dataset[[i]] <- (dataset[[i]] - min) / ((max - min)/10)
      }
    }

  return(dataset)

}
