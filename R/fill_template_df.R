#' Fill DF Helper
#'
#' Fills the empty template df with the function data.
#'
#' @return Dataframe with polarization data from polaR functions
#' @export
#'
#' @examples fill_template_df(df, "cses_5", "sd_mass", "self", "SD Attitudes")
fill_template_df <- function(dataset, dataset_string, measure, issue, label_measure, source = NA){

  # Create Empty DF with template function
  df <- empty_df(nrow = nrow(dataset))

  # Copy dataset to template df
  df[["country"]] <- dataset[["country"]]
  df[["year"]] <- dataset[["year"]]
  df[["pol_score"]] <- dataset[["pol_score"]]

  df[["measure"]] <- paste0(measure)
  df[["dataset"]] <- dataset_string
  df[["issue"]] <- issue
  df[["label_measure"]] <- label_measure
  df[["source"]] <- source

  return(df)

}
