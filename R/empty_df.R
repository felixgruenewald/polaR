#' Empty DF Helper
#'
#' Creates an empty template DF with the right n of rows to fill with polarization data.
#'
#' @return Empty DF with n rows and 10 columns formatted for use in polaR functions
#' @export
#'
#' @examples empty_df(10)
empty_df <- function(nrow){

  # Define Columns
  cols_df <- c("country",
               "year",
               "pol_score",
               "measure",
               "dataset",
               "issue",
               "label_measure",
               "label_data",
               "label",
               "source")

  # country       =
  # year          =
  # pol_score     = Polarization Score
  # measure       = Measure ID
  # dataset       = Dataset ID
  # issue         = Issue Dimension of computed score
  # label_measure = Labelled Measure
  # label_data    = Labelled Data
  # label         = Full label (measure + dataset)
  # source        = Source of information (survey responses, expert opinion)

  df <- data.frame(matrix(ncol=length(cols_df),nrow=nrow, dimnames=list(NULL, cols_df)))

  return(df)

}
