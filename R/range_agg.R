#' Aggregate Range
#'
#' Range of party scores on the aggregate level.
#' Other than the individual range score, this computes the party means first and only then computes the range.
#' For more information, see the \href{https://polarization.wiki/measures/range}{encyclopedia entry on the Range Between Outer-Right and -Left Parties}
#'
#'
#' Compatible Datasets: CSES
#'
#' @param dataset Dataframe
#' @param issue Issue dimension
#'
#' @return Country/year dataset wih range score.
#'
#' @examples cses_5_range <- range_agg(cses_5, "leftright")
#'
#' @importFrom dplyr group_by summarise_at select
#'
#' @export
range_agg <- function(dataset, issue, dataset_string = NA){

  measure <- "range_agg"
  label_measure <- "Range (Aggregate Level)"

  if (is.na(dataset_string)){
    dataset_string <- deparse(substitute(dataset))
  }

  # take measure and form variable names
  all_parties <- c("_party_A", "_party_B", "_party_C", "_party_D", "_party_E", "_party_F", "_party_G", "_party_H", "_party_I")
  all_parties <- paste(issue, all_parties, sep="")

  dataset <- dataset %>%
    select(country, year, all_of(all_parties)) %>%
    group_by(country, year) %>%
    summarise_at(vars(all_parties), mean, na.rm = TRUE)

  # Only apply max function when not all values are NA, thanks to coffeinjunky@stackoverflow
  my.max <- function(x) ifelse( !all(is.na(x)), max(x, na.rm=T), NA)
  my.min <- function(x) ifelse( !all(is.na(x)), min(x, na.rm=T), NA)

  # Min and Max ratings of parties
  dataset["min_party"] <- apply(dataset[, all_parties], 1, my.min)
  dataset["max_party"] <- apply(dataset[, all_parties], 1, my.max)

  # Compute Range
  dataset["pol_score"] <- dataset["max_party"] - dataset["min_party"]

  df <- fill_template_df(dataset, dataset_string, measure, issue, label_measure)

  return(df)
}
