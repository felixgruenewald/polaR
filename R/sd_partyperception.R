#' Standard Deviation of Party Perception
#'
#' Standard deviation of individual level party evaluations. Can be aggregated to country/year level.
#' For more information, see the \href{https://polarization.wiki/measures/sd}{encyclopedia entry on Standard Deviation}.
#'
#' Compatible Datasets: CSES
#'
#' @param dataset Dataframe
#' @param issue Issue dimension
#' @param aggregate Aggregate to country/year level
#'
#' @return Dataset wih individual level sd score
#' @export
#'
#' @examples cses <- sd_parties(cses, "leftright")
sd_partyperception <- function(dataset, issue, aggregate){

  measure <- "sd_partyperception"
  label_measure = "SD Party Perception"
  dataset_string <- deparse(substitute(dataset))

  # take measure and form variable names
  all_parties <- c("_party_A", "_party_B", "_party_C", "_party_D", "_party_E", "_party_F", "_party_G", "_party_H", "_party_I")
  all_parties <- paste(issue, all_parties, sep ="")

  dataset["partynumber"] <- apply(dataset[all_parties], MARGIN = 1, function(x) sum(!is.na(x)))

  # SD of party perception on indiv. level
  dataset[measure] <- apply(dataset[, all_parties], 1, sd, na.rm = TRUE)

  # All Inf to NA
  #dataset[sapply(dataset, is.infinite)] <- NA

  if (aggregate == FALSE){
    return(dataset)
  } else if (aggregate == TRUE){
    dataset_agg <- mean_aggregate(dataset, dataset_string, measure, issue, label_measure)
    return(dataset_agg)
  }

}
