% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/polaR_import.R
\name{polaR_import}
\alias{polaR_import}
\title{polaR Dataset Import}
\usage{
polaR_import(source, path, keep_all = TRUE)
}
\arguments{
\item{source}{The source of the data (cses_imd, cses_5, ess, eb...)}

\item{path}{File path string to a .dta file}

\item{keep_all}{defaults to TRUE, keep all variables or reduce the dataset size to the package relevant variables}
}
\value{
Dataframe that is ready for use with other functions
}
\description{
Imports datasets and puts them into a coherent format for use with the polaR package.
Takes the dta files form the original sources and renames and formats package-relevant variables.
}
\section{Source}{
Different datasets use different variable names and scales for the same items. The package therefore contains an import function that takes a range of datasets and recodes names, scales and missing values into a consistent pattern on the base of which the measures can be computed.

For this, a pre-defined dictionary containing all relevant variables is used to look up the variable names for the dataset specified under the \code{source} option, and transforms them accordingly.

As of now, the following datasets are available:\tabular{lr}{
   Dataset \tab \code{source} \cr
   CSES IMD \tab \code{cses_imd} \cr
   CSES 5 \tab \code{cses_5} \cr
   ESS \tab \code{ess} \cr
   CHES \tab \code{ches} \cr
   MARPOR \tab \code{cmp} \cr
   Eurobarometer \tab \code{eb} \cr
}


\code{polaR::var_dict} shows the renaming pattern and variables used for this. You can load and adapt or add to the dictionary to increase the number of variables or datasets compatible with the package.
\subsection{ESS}{

\emph{Note}: The ESS is a difficult case, as the waves are available individually. Some of these waves do not contain the relevant variables for the polaR functions and therefore, the measures won't compute on these datasets. To generate the data from the encyclopedia, we merged the 10 waves together:

\if{html}{\out{<div class="sourceCode r">}}\preformatted{ess1  <- polaR_import(source = "ess", path = "path/to/ess1.dta", keep_all = F)
ess2  <- polaR_import(source = "ess", path = "path/to/ess2.dta", keep_all = F)

...

ess10 <- polaR_import(source = "ess", path = "path/to/ess10.dta", keep_all = F)
ess <-  rbind(ess1, ess2, ess3, ess4, ess5, ess6, ess7, ess8, ess9, ess10)
rm(ess1, ess2, ess3, ess4, ess5, ess6, ess7, ess8, ess9, ess10)
}\if{html}{\out{</div>}}
}
}

\examples{
polaR_import(source = "cses_imd", path = "data/cses_imd.dta")
}
