% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/distance.R
\name{distance}
\alias{distance}
\title{Distance}
\usage{
distance(dataset, weighted, aggregate)
}
\arguments{
\item{dataset}{Imported Dataset with adapted Varnames, originally CSES IMD}

\item{weighted}{Logical to switch between weighted and unweighted measure.}

\item{aggregate}{Logical to switch between individual and aggregated output.}
}
\value{
Dataset with added variables distance and distance_wgt
}
\description{
Calculate weighted and unweighted Distance measure.
For more information, see the \href{https://polarization.wiki/measures/distance}{encyclopedia entry on (Weighted) Mean Distance From the Most-Liked Party}
}
\details{
Compatible Datasets: CSES
}
\examples{
cses <- distance(cses, weighted = TRUE, aggregate = TRUE)

}
